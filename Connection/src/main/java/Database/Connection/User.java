package Database.Connection;

public class User {

	int id;
	String name;
	String address;
	int age;
	double salary;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public User(int id, String name, String address, int age, double salary) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.age = age;
		this.salary = salary;
	}

	public User(String name, String address, int age, double salary) {
		this.name = name;
		this.address = address;
		this.age = age;
		this.salary = salary;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public void ResponsePOJO(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public void setAddress(String address) {
		// TODO Auto-generated method stub
		this.address = address;
	}

}

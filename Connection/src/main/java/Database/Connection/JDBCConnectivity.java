package Database.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCConnectivity {

	public static void main(String[] args) {
		try {
			// Class.forName("org.postgresql.Driver");

			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");

			if (con != null) {
				System.out.println("Connected to Database");
			} else {
				System.out.println("Not connected");
			}
			Statement stmt = con.createStatement();

			// String sql="insert into company(id,name,age,address,salary) values (3,'Chetan
			// ji',30,'Indore',12000)";

			String sql = "delete from company where id=1";
			int x = stmt.executeUpdate(sql);
			if (x > 0) {
				System.out.println("Successfully deleted!!!!!!!!!!");
			} else {
				System.out.println("Successfully  not inserted!!!!!!!!!!");
			}

			// ResultSet rs = stmt.executeQuery("select * from company");

//			while (rs.next()) {
//				System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getInt(3) + " " + rs.getString(4)
//						+ " " + rs.getString(5));
//				con.close();
//			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

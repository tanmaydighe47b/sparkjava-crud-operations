package Database.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class CrudJdbc {

	public static Map users = new HashMap<Object, Object>();
	private static final AtomicInteger count = new AtomicInteger(0);

	public ResponsePOJO findById(String id) {
		List<User> record = new ArrayList<User>();
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");
			Statement stmt = con.createStatement();
			String sql = "select * from company where id='" + id + "'";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				User newUser = new User();
				newUser.setId(rs.getInt("id"));
				newUser.setName(rs.getString("name"));
				newUser.setAge(rs.getInt("age"));
				newUser.setAddress(rs.getString("address"));
				newUser.setSalary(rs.getFloat("salary"));
				record.add(newUser);
			}
		} catch (SQLException e) {
			e.getMessage();
		}
		if (record.isEmpty()) {
			return new ResponsePOJO(true, "No Data Found Check ID", null);
		} else {
			return new ResponsePOJO(false, "Data Retrieved Successfully", record);
		}
	}

	public ResponsePOJO update(String id, User user) {
		List<User> record = new ArrayList<User>();
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");
			String sql = "UPDATE COMPANY " + "SET name=?,age=? where id=" + id;
			PreparedStatement pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, user.name);
			pstmt.setInt(2, user.age);
			pstmt.executeUpdate();
			ResultSet ids = pstmt.getGeneratedKeys();
			if (ids.next()) {
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from company where id=" + ids.getInt("id"));
				while (rs.next()) {
					User newUser = new User();
					newUser.setId(rs.getInt("id"));
					newUser.setName(rs.getString("name"));
					newUser.setAge(rs.getInt("age"));
					newUser.setAddress(rs.getString("address"));
					newUser.setSalary(rs.getFloat("salary"));
					record.add(newUser);
				}
			}
			con.close();
		} catch (SQLException e) {
			e.getMessage();
		}
		if (record.isEmpty()) {
			return new ResponsePOJO(true, "No Data Updated", null);
		} else {
			return new ResponsePOJO(false, "Data Updated  Successfully", record);
		}
	}

	public ResponsePOJO add(User user) {
		int recId = 0;
		List<User> record = new ArrayList<User>();
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");
			String sql = "INSERT INTO company(name,age,address,salary) " + "VALUES(?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, user.getName());
			pstmt.setInt(2, user.getAge());
			pstmt.setString(3, user.getAddress());
			pstmt.setDouble(4, user.getSalary());
			pstmt.executeUpdate();
			ResultSet ids = pstmt.getGeneratedKeys();
			if (ids.next())
				recId = ids.getInt("id");
			if (recId > 0) {
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select * from company where id=" + recId);
				while (rs.next()) {
					User newUser = new User();
					newUser.setId(rs.getInt("id"));
					newUser.setName(rs.getString("name"));
					newUser.setAge(rs.getInt("age"));
					newUser.setAddress(rs.getString("address"));
					newUser.setSalary(rs.getFloat("salary"));
					record.add(newUser);
				}
			}
			con.close();
		} catch (SQLException e) {
			e.getMessage();
		}
		if (record.isEmpty()) {
			return new ResponsePOJO(true, "No Data Inserted", null);
		} else {
			return new ResponsePOJO(false, "Data Inserted  Successfully", record);
		}
	}

	public ResponsePOJO delete(String id) {
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");
			String sql = "DELETE FROM company " + "WHERE id = '" + id + "'";
			PreparedStatement pstmt = con.prepareStatement(sql);
			int rs = pstmt.executeUpdate();
			if (rs > 0) {
				return new ResponsePOJO(false, "Data Deleted Successfully", null);
			} else {
				return new ResponsePOJO(true, "No Data deleted Check ID", null);
			}
		} catch (SQLException e) {
			e.getMessage();
		}
		return new ResponsePOJO(true, "Something Went wrong", null);
	}

	public ResponsePOJO findAll() throws SQLException, ClassNotFoundException {
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
				"postgres123");
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from company");
		List<User> record = new ArrayList<User>();
		while (rs.next()) {
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setName(rs.getString("name"));
			user.setAge(rs.getInt("age"));
			user.setAddress(rs.getString("address"));
			user.setSalary(rs.getFloat("salary"));
			record.add(user);
		}
		if (record.isEmpty()) {
			return new ResponsePOJO(true, "No Data Found", null);
		} else {
			return new ResponsePOJO(false, "", record);
		}
	}

	public static void connectivity() {
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:2345/postgres", "postgres",
					"postgres123");
		} catch (SQLException e) {
			e.getMessage();
		}
	}
}

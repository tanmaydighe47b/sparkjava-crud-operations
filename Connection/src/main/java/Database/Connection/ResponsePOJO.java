package Database.Connection;

import java.util.List;

public class ResponsePOJO {

	Boolean isError;
	String erroMessage;
	List<User> data;

	public ResponsePOJO(Boolean isError, String erroMessage, List<User> record) {
		this.isError = isError;
		this.erroMessage = erroMessage;
		this.data = record;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getErroMessage() {
		return erroMessage;
	}

	public void setErroMessage(String erroMessage) {
		this.erroMessage = erroMessage;
	}

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}

}

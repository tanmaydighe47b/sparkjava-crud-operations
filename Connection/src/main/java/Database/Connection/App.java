// This is App.java
package Database.Connection;

import static spark.Spark.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import spark.Route;

public class App {
	private static CrudJdbc userService = new CrudJdbc();
	private static ObjectMapper om = new ObjectMapper();

	public static void main(String[] args) {
		port(9898);

		get("/", (req, res) -> "Welcome To CRUD Operations for Company Using JDBC Connectivity");

		get("/user", (req, res) -> {
			return om.writeValueAsString(userService.findAll());
		});

		get("/user/:id", (req, res) -> {
			String id = req.params(":id");
			ResponsePOJO result = userService.findById(id);
			if (result == null) {
				return om.writeValueAsString("NO USERS FOUND....");
			} else {
				return om.writeValueAsString(result);
			}

		});
		post("/user/add", (req, res) -> {
			Map<String, Object> mapReq = dataToMap(req.body());
			User user = new User();
			try {
				user.setName((String) mapReq.get("name"));
				user.setAddress((String) mapReq.get("address"));
				user.setAge((int) mapReq.get("age"));
				user.setSalary((double) mapReq.get("salary"));
			} catch (Exception e) {
				return om.writeValueAsString(new ResponsePOJO(true, "Parameter Wrong", null));
			}
			ResponsePOJO responsePOJO = userService.add(user);
			if (responsePOJO.getIsError())
				res.status(400);
			else
				res.status(201);
			return om.writeValueAsString(responsePOJO);
		});
		put("/user/update/:id", (req, res) -> {
			String id = req.params(":id");
			User user = new User();
			try {
				Map<String, Object> mapReq = dataToMap(req.body());
				user.setName((String) mapReq.get("name"));
				user.setAddress((String) mapReq.get("address"));
				user.setAge((int) mapReq.get("age"));
				user.setSalary((double) mapReq.get("salary"));
			} catch (Exception e) {
				return om.writeValueAsString(new ResponsePOJO(true, "Parameter Wrong", null));
			}
			ResponsePOJO responsePOJO = userService.update(id, user);
			if (responsePOJO.getIsError())
				res.status(400);
			else
				res.status(201);
			return om.writeValueAsString(responsePOJO);
		});
		delete("/user/delete/:id", (req, res) -> {
			String id = req.params(":id");
			ResponsePOJO user = userService.findById(id);
			ResponsePOJO responsePOJO = userService.delete(id);
			if (responsePOJO.getIsError())
				res.status(400);
			else
				res.status(201);
			return om.writeValueAsString(responsePOJO);
		});
	}

	public static Map<String, Object> dataToMap(String data) {
		Map<String, Object> map = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			map = mapper.readValue(data, Map.class);
		} catch (Exception e) {
			throw new RuntimeException("IOEXception while mapping object (" + data + ") to JSON");
		}
		return map;
	}
}
